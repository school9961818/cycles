﻿/*
 * Cyklus "for" se používá v případě, že dopředu známe počet opakování
 */

for (int i = 0; i < 10; i = i + 1)
{
    Console.Write(i + ", ");
}
Console.WriteLine();

/*
 * Protože přičítání a odečítání 1 se v programování používá často,
 * můžeme v C# použít zkrácený zápis
 *   i = i + 1;   -->    i++;
 *   i = i - 1;   -->    i--;
 */

/*
 * Zkuste napsat "for" a poté dvakrát "Tab"
 * Uvnitř kulatých závorek je následující
 * 
 *    (inicializace ; podmínka ukončení cyklu ; akce provedená po každém opakování)
 * 
 * Upravte svůj cyklus tak, aby začínal na 10 a postupně klesal až k 0.
 * Vypisujte hodnotu v "i" na obrazovku
 */


/*
 * Napište cyklus, který vypíše na obrazovku sudá čísla od 0 do 20ti
 * Zkuste se zamyslet (k cestě vede vždy více cest)
 * Návrhy
 *   - Cyklit všechna čísla a kontrolovat, jestli je číslo sudé/liché
 *   - Upravit cyklus tak, aby necyklil po jednom, ale po dvou a vyhnout se kontrole
 */


/*
 * Napište cyklus, který na obrazovku 10x vypíše "Platit daně je normální."
 */


/*
 * Napište cyklus, který na obrazovku 30x vypíše (ve 3 sloupcích) "Nejhorší rada je ta nevyžádaná."
 * 
 *   Pozn. Bez použití "Console.Writeline("Nejhorší rada je ta nevyžádaná. Nejhorší rada je ta nevyžádaná. Nejhorší rada je ta nevyžádaná.");
 *        (Pamatujete ještě na Console.Write();?)
 */


/*
 * BONUS: Upravte předchozí cyklus tak, aby před každým sdělením bylo jeho číslo
 * Output:
 *   0. Nejhorší rada je ta nevyžádaná. 10. Nejhorší rada je ta nevyžádaná. 20. Nejhorší rada je ta nevyžádaná.
 *   1. Nejhorší rada je ta nevyžádaná. 11. Nejhorší rada je ta nevyžádaná. 21. Nejhorší rada je ta nevyžádaná.
 *   ...
 *   9. Nejhorší rada je ta nevyžádaná. 19. Nejhorší rada je ta nevyžádaná. 29. Nejhorší rada je ta nevyžádaná. 
 */



