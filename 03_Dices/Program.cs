int luckyNumber = new Random().Next(100);

Console.WriteLine("Today's lucky number is " + luckyNumber + ".");
Console.WriteLine("Today's lucky number is {0}.", luckyNumber);
Console.WriteLine($"Today's lucky number is {luckyNumber}.");

/*
 * Také u výpisu na obrazovku existuje více než jedna cesta.
 * Zde ukázka tří možností (je jich ještě více).
 * Každá má svá specifika (nedá se říct, že TA či ONA je jednoznačně nejlepší).
 * Já od této chvíle budu používat jen třetí způsob - https://learn.microsoft.com/cs-cz/dotnet/csharp/language-reference/tokens/interpolated
 * Jak budete vypisovat vy je jen na Vás.
 */

/*
 * Napište kód, který bude simulovat hod 20-ti stěnnou kostkou.
 * Podle výsledku vypište na obrazovku.
 *   1-5 - Smrt.
 *   6-18 - Zranění.
 *   19-20 - Úspěšná obrana.
 */


/*
 * Napište kód, který bude simulovat 10 hodů 6ti stěnnou kostkou. Jednotlivé výsledky vypíše na obrazovku.
 */


/*
 * Napište kód, který bude simulovat 10 hodů 6ti stěnnou kostkou a počítat, kolikrát padne 6ka. 
 * Jednotlivé výsledky vypíše na obrazovku.
 * Nakonec vypište, kolik celkem padlo 6tek.
 */

/*
 * Napište kód, který bude simulovat 10 hodů dvěma 6ti stěnnými kostkami. Bude počítat, kolikrát padla stejná čísla. 
 * Jednotlivé výsledky vypíše na obrazovku.
 * Nakonec vypište, kolikrát na kostkách padlo stejné číslo.
 */

/*
 * BONUS: Napište kód simulující souboj bojovníka s příšerou. Oba začínají s deseti životy.
 * Boj končí, když bojovník nebo příšera mají životů 0. 
 * Každé kolo se hodí 6ti stěnnou kostkou
 *   1-3 - bojovník ztrácí život
 *   4-5 - příšera ztrácí život
 *   6 - bojovník získává dva životy zpátky
 * Průběh boje vypisujte na obrazovku.
 */
