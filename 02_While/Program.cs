/*
 * Toto je příkaz, který vygeneruje náhodné číslo od 0 do 9
 */

int randomNumber;
randomNumber = new Random().Next(0, 10);


/*
 * Cyklus while používáme, když nevíme dopředu, kolikrát se má opakovat.
 * Zase platí dvakrát "Tab" při psaní
 */

while (randomNumber != 9)
{
    Console.WriteLine($"{randomNumber} - Miss.");
    randomNumber = new Random().Next(0, 10);
}

Console.WriteLine($"{randomNumber} - Hit");

/*
* Zkuste program několikrát spustit. Cyklus provede náhodný počet opakování.
* Oproti cyklu "for" musíme dávat větší bacha
*  - zařídit sami inicializaci
*  - zařídit sami, že cyklus skončí
* Schválně zkuste upravit generování uvnitř cyklu na ".Next(0, 9);" a program spustit.
* Tzv. nekonečná smyčka, protože takový příkaz generuje jen v rozsahu 0-8 a devítka tak nikdy nepadne.
*/


/*
* Vytvořte novou proměnnou typu int a uložte do ní číslo 2
* Pomocí cyklu vypište na obrazovku mocniny dvou menší než 3000.
*    "2, 4, 8, 16,..."
*/


/*
* Pomocí cyklu vypisujte čísla od 1 nahoru, dokud nenarazíte na číslo dělitelné 5 a 7 zároveň
*/



/*
* Cyklus "Do-While" prosím nepoužívejte. Nikdy.
* Koho by zajímal důvod více do hloubky, tak např. zde - https://www.reddit.com/r/learnprogramming/comments/93gka0/why_dont_we_see_more_use_of_the_dowhile_loop/ 
* Nebudu ho tedy ani ukazovat.
* 
* Existuje ješte cyklus "foreach", který je velmi užitečný, ale ten si ukážeme v pozdějších lekcích.
*/


/*
 * BONUS: Pomocí cyklu vypište Fibbonacciho posloupnost pro čísla menší jak 2000.
 */
