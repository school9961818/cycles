## Prerequisites

[Conditions](https://gitlab.com/school9961818/conditions/)

## Goal

* Code
  * For/While cycle
  * Random numbers
  * String interpolation

## Diffuculty

* Beginners
* 2-3x45 min

## Instructions

  

Cykly umožňují algoritmům opakovat určité akce. Díky tomu může být kód mnohem kratší a přehlednější

```mermaid

flowchart TD

Start --> IF{Condition}

IF --> |Yes| Action

Action --> IF

IF --> |No| Stop

```

  

Cyklus provádí akci do té doby, dokud je splněna jeho podmínka. 

Naklonujte si projekt do počítače a postupně vyřešte zadání ve všech projektech.

  
  

## Feedback

https://forms.gle/bnLC9cBzckNMnqv36

## Where to next

[Methods](https://gitlab.com/school9961818/methods)
